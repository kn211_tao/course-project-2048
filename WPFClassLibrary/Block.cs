using System.Windows.Media; // ��� ���������
using System.Collections.Generic; // ��� ������

namespace WPFClassLibrary
{
    public class Block
    {
        protected bool isCombined; // �� �'�������� �����
        public bool IsCombined
        {
            get { return isCombined; }
            set { isCombined = value; }
        }
        protected bool isNewBlock; // �� �� ����� ����
        public bool IsNewBlock
        {
            get { return isNewBlock; }
            set { isNewBlock = value; }
        }
        protected int valuee; // �������� �����
        public int Value
        {
            get { return valuee; }
            set { valuee = value; }
        }

        public Block() // ����������� �� ������������
        {
            IsCombined = false;
            IsNewBlock = false;
            valuee = 0;
        }

        #region ������� ������
        private static readonly SolidColorBrush block2 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EEE4DA"));
        private static readonly SolidColorBrush block4 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDE0C8"));
        private static readonly SolidColorBrush block8 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F2B179"));
        private static readonly SolidColorBrush block16 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F59563"));
        private static readonly SolidColorBrush block32 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F67C5F"));
        private static readonly SolidColorBrush block64 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F65E3B"));
        private static readonly SolidColorBrush block128 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDCF72"));
        private static readonly SolidColorBrush block256 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDCC61"));
        private static readonly SolidColorBrush block512 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC850"));
        private static readonly SolidColorBrush block1024 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC53F"));
        private static readonly SolidColorBrush block2048 = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#EDC22E"));
        private static readonly SolidColorBrush blockSuper = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#3C3A32"));

        private static readonly SolidColorBrush NoneBlock = new SolidColorBrush((Color)ColorConverter.ConvertFromString("LightGray"));

        public SolidColorBrush GetTitleBlocksColor(Block blk)
        {
            switch (blk.valuee)
            {
                case 2:
                    return block2;
                case 4:
                    return block4;
                case 8:
                    return block8;
                case 16:
                    return block16;
                case 32:
                    return block32;
                case 64:
                    return block64;
                case 128:
                    return block128;
                case 256:
                    return block256;
                case 512:
                    return block512;
                case 1024:
                    return block1024;
                case 2048:
                    return block2048;
                default:
                    return blockSuper;

            }
        }
        public static SolidColorBrush GetBlocksNoneTitleColor()
        {
            return NoneBlock;
        }
        #endregion

        #region �������� ���� ������
        static private int maxLenght = 2;
            private static List<int> ProcessList(ref List<int> ls, ref GameLogic game)  // ���� ����� ��������, �� ��������� ������ ��������� �����
            {
                List<int> result = new List<int>();

                maxLenght = game.Size - 1;
                // ������� �������� : 0202 -> 0400 
                int front = 0, end = 1;
                while (end <= maxLenght && front <= maxLenght - 1) // 2 � 1
                {
                    if (ls[front] == 0)
                    {
                        front++;
                        end = front + 1;
                    }
                    else if (ls[end] == 0)
                    {
                        end++;
                    }
                    else
                    {
                        if (ls[front] == ls[end])  // ���� � �� �������
                        {
                            result.Add(front); // ������ ������� ��� ���������                       
                            ls[front] = ls[front] * 2; // ������� �� ��������� ������
                            ls[end] = 0;
                            game.Score = game.Score + ls[front]; // ������ ����
                            front++;
                            end = front + 1;
                        }
                        else
                        {
                            front++;
                            end = front + 1;
                        }
                    }
                }
                // ����������� ��� : 0400 -> 4000
                int ZeroIndex = -1;
                // ����������� ��� ����� ������ (����� �����-1)
                for (int i = 0; i < game.Size; i++)
                {
                    if (ls[i] == 0)
                    {
                        if (ZeroIndex == -1)
                            ZeroIndex = i;
                    }
                    else
                    {
                        if (ZeroIndex != -1)
                        {
                            if (result.Contains(i))
                            {
                                result[result.IndexOf(i)] = ZeroIndex;
                            }
                            ls[ZeroIndex] = ls[i];
                            ls[i] = 0;
                            ZeroIndex = -1;
                            i = ZeroIndex + 1;
                            result.Add(-1);
                        }
                    }
                }
                return result;
            }

        public static bool TryUp(ref GameLogic game, ref GameLogic oldgame) // ���� ����� ��������, ������� true
        {
            oldgame = new GameLogic(game); // �������� ��� ����

            //��������� ��������� ���� �����, ��� �������� ����� �� �� ��������
            Map tmpmap = new Map(game.map);
            int tmpscore = game.Score; // ��������� ��������� ����
           
            bool IsBlocksChanged = false;
            for (int x = 0; x < game.Size; x++)
            {
                List<int> ls = new List<int>();
                for (int y = 0; y < game.Size; y++)
                {
                    ls.Add(game.map.GetValue(x, y));
                    // ��������� ���������� �� ��������� � �� ���� ������
                    game.map.grid[x, y].IsCombined = false;
                    game.map.grid[x, y].IsNewBlock = false;
                }

                List<int> ChangedList = ProcessList(ref ls, ref game);
                if (ChangedList.Count != 0) // ���� ���� �������
                {
                    IsBlocksChanged = true;
                    // ��������� �� ���� ������� (���������� ��� ������� ����)
                    
                    for (int i = 0; i < ChangedList.Count; i++)
                    {
                        if (ChangedList[i] != -1)
                            game.map.grid[x, ChangedList[i]].IsCombined = true;
                    }
                    // ��������� �����
                    int ls_k = 0;
                    for (int y = 0; y < game.Size; y++) // (���������� ��� ������� ����)   
                    {
                        game.map.grid[x, y].valuee = ls[ls_k];
                        ls_k++;
                    }
                    ls.Clear();
                }
            }

            // ���� ����� ��������, ��� ��������� ���� ����� � ������������ �� ����� ���� �� �����
            if (IsBlocksChanged == true)
            {
               oldgame.Score = tmpscore;
                oldgame.map = new Map(tmpmap);
            }
            return IsBlocksChanged;
        }
        public static bool TryDown(ref GameLogic game, ref GameLogic oldgame) // ���� ����� ��������, ������� true
        {
            oldgame = new GameLogic(game); // �������� ��� ����

            //��������� ��������� ���� �����, ��� �������� ����� �� �� ��������
            Map tmpmap = new Map(game.map);

            bool IsBlocksChanged = false;
            for (int x = 0; x < game.Size; x++)
            {
                List<int> ls = new List<int>();
                for (int y = game.Size - 1; y >= 0; y--)
                {
                    ls.Add(game.map.GetValue(x, y));
                    // ��������� ���������� �� ��������� � �� ����� ����
                    game.map.grid[x, y].IsCombined = false;
                    game.map.grid[x, y].IsNewBlock = false;
                }
                List<int> ChangedList = ProcessList(ref ls, ref game);
                if (ChangedList.Count != 0) // If blocks changed
                {
                    IsBlocksChanged = true;
                    // ��������� �� ���� ������� (���������� ��� ������� ����)

                    for (int i = 0; i < ChangedList.Count; i++)
                    {
                        if (ChangedList[i] != -1)
                            game.map.grid[ChangedList[i], x].IsCombined = true;
                    }
                    int ls_k = 0;
                    for (int y = game.Size - 1; y >= 0; y--)
                    {
                        game.map.grid[x, y].valuee = ls[ls_k];
                        ls_k++;
                    }
                }
                ls.Clear();
            }

            // ���� ����� ��������, ��� ��������� ���� ����� � ������������ �� ����� ���� �� �����
            if (IsBlocksChanged == true)
            {
                oldgame.map = new Map(tmpmap);
            }
            return IsBlocksChanged;
        }
        public static bool TryRight(ref GameLogic game, ref GameLogic oldgame) // ���� ����� ��������, ������� true
        {
            oldgame = new GameLogic(game); // �������� ��� ����

            // ��������� ��������� ���� �����, ��� �������� ����� �� �� ��������
            Map tmpmap = new Map(game.map);

            bool BlocksChanged = false;
            for (int y = 0; y < game.Size; y++)
            {
                List<int> ls = new List<int>();
                for (int x = game.Size - 1; x >= 0; x--)
                {
                    ls.Add(game.map.GetValue(x, y));
                    // ��������� ���������� �� ��������� � �� ����� ����
                    game.map.grid[x, y].IsCombined = false;
                    game.map.grid[x, y].IsNewBlock = false;
                }
                List<int> ChangedList = ProcessList(ref ls, ref game);
                if (ChangedList.Count != 0) // ���� ����� ��������
                {
                    BlocksChanged = true;

                    // ��������� �� ���� ������� (���������� ��� ������� ����)

                    for (int i = 0; i < ChangedList.Count; i++)
                    {
                        if (ChangedList[i] != -1)
                            game.map.grid[ChangedList[i], y].IsCombined = true;
                    }
                    int ls_k = 0;
                    for (int x = game.Size - 1; x >= 0; x--)
                    {
                        game.map.grid[x, y].valuee = ls[ls_k];
                        ls_k++;
                    }
                    ls.Clear();
                }
            }

            // ���� ����� ��������, ��� ��������� ���� ����� � ������������ �� ����� ���� �� �����
            if (BlocksChanged == true)
            {
                oldgame.map = new Map(tmpmap);
            }
            return BlocksChanged;
        }
        public static bool TryLeft(ref GameLogic game, ref GameLogic oldgame) // ���� ����� ��������, ������� true
        {
            oldgame = new GameLogic(game); // �������� ��� ����

            //��������� ��������� ���� �����, ��� �������� ����� �� �� ��������
            Map tmpmap = new Map(game.map);

            bool BlocksChanged = false;
            for (int y = 0; y < game.Size; y++)
            {
                List<int> ls = new List<int>();
                for (int x = 0; x < game.Size; x++)
                {
                    ls.Add(game.map.GetValue(x, y));
                    // ��������� ���������� �� ��������� � �� ����� ����
                    game.map.grid[x, y].IsCombined = false;
                    game.map.grid[x, y].IsNewBlock = false;
                }
                List<int> ChangedList = ProcessList(ref ls, ref game);
                if (ChangedList.Count != 0) // ���� ����� ��������
                {
                    BlocksChanged = true;

                    // ��������� �� ���� ������� (���������� ��� ������� ����)
                    for (int i = 0; i < ChangedList.Count; i++)
                    {
                        if (ChangedList[i] != -1)
                            game.map.grid[ChangedList[i], y].IsCombined = true;
                    }
                    int ls_k = 0;
                    for (int x = 0; x < game.Size; x++)
                    {
                        game.map.grid[x, y].valuee = ls[ls_k];
                        ls_k++;
                    }
                    ls.Clear();
                }
            }

            // ���� ����� ��������, ��� ��������� ���� ����� � ������������ �� ����� ���� �� �����
            if (BlocksChanged == true)
            {
                oldgame.map = new Map(tmpmap);
            }
            return BlocksChanged;
        }
        #endregion
    }
}
