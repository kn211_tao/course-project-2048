﻿
namespace WPFClassLibrary
{
    public class Map
    {
        public int Size { get;  set; }
        public Block[,] grid; // сітка з блоків

        public Map() // конструктор для карти по замовчуванню
        {
            Size = 4;
            grid = new Block[Size, Size];

            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    grid[x, y] = new Block();
                }
            }
        }
        public Map(int size) // конструктор для карти
        {
            Size = size;
            grid = new Block[size, size];

            for (int x = 0; x < grid.GetLength(0); x++)
            {
                for (int y = 0; y < grid.GetLength(1); y++)
                {
                    grid[x, y] = new Block();
                }
            }
        }
        public Map(Map obj) //конструктор копіювання
        {
            Size = obj.Size;
            grid = obj.grid;
        }

        public int GetValue(int x, int y) // геттер метод отримання значення з карти
        {
            if (x < Size && x >= 0 && y < Size && y >= 0)
                return grid[x, y].Value;
            else return -1;
        }
        public void SetValue(int x, int y, int value) // сеттер метод встановлення значення на карту
        {
            if (x < Size && x >= 0 && y < Size && y >= 0)
                grid[x, y].Value = value;
        }

    }
}