﻿using System;
using FileClassLibrary;

namespace WPFClassLibrary
{
    public class GameLogic
    {
        private int size; // розмір карти
        public int Size
        {
            get { return size; }
            set { size = value; }
        }

        private bool isGameEnd; // чи закінчилась гра
        public bool IsGameEnd  
        {
            get { return isGameEnd; }
            set { isGameEnd = value; }
        }

        private bool wasMoveTo; // чи змінювалося положення
        public bool WasMoveTo 
        {
            get { return wasMoveTo; }
            set { wasMoveTo = value; }
        }

        private int countZero; // кількість порожніх плиток
        public int CountZero
        {
            get { return countZero; }
            set { countZero = value; }
        }

        private int score; // бали
        public int Score
        {
            get { return score; }
            set { score = value; }
        }

        public Player Player { get; } // гравець

        Random rnd = new Random();
        public Map map = new Map();

        public GameLogic(int size, Map gamemap, Player player) // конструктор для гри
        {
            Size = size;
            score = 0;
            countZero = Size * Size;
            IsGameEnd = false;
            WasMoveTo = false;
            map = new Map(gamemap);
            Player = player;
        }
        public GameLogic(GameLogic obj) // конструктор копіювання для гри
        {
            Size = obj.Size;
            score = obj.Score;
            countZero = obj.CountZero;
            IsGameEnd = false;
            WasMoveTo = obj.WasMoveTo;
            map = new Map(obj.map);
            Player = obj.Player;
        }
        public GameLogic(int size) // конструктор для гри
        {
            map = new Map(size);
            countZero = Size * Size;
            Start(size);

        }

        public void Start(int size) //початок та ініціалізація карти
        {
            score = 0;
            countZero = size * size;
            IsGameEnd = false;
            WasMoveTo = false;
            for (int x = 0; x < Size; x++)
                for (int y = 0; y < Size; y++)
                    map.SetValue(x, y, 0);
        }

        public void AddRandomValue(ref Map gamemap) // додавання випадково числа 2 або 4
        {
            if (IsGameEnd) return;
            while (true)                                // Метод шукає випадковий пустий блок
            {                                           // Без while випадковий блок іноді встає на інший 
                int x = rnd.Next(0, gamemap.Size);
                int y = rnd.Next(0, gamemap.Size);
                if (gamemap.GetValue(x, y) == 0)
                {
                    if (rnd.NextDouble() > 0.2) // 2 з ймовірністю 80% та 4 з 20%
                        gamemap.SetValue(x, y, 2);
                    else
                        gamemap.SetValue(x, y, 4);

                    countZero = CountBlocksNumberZero(ref gamemap);
                    gamemap.grid[x, y].IsNewBlock = true;
                    break;
                }
            }
            return;
        }
        public int GetValueFromMap(int x, int y) // метод отримання значення з карти
        {
            return map.GetValue(x, y);
        }
        private int CountBlocksNumberZero(ref Map gamemap) // рахування кількості пустих плиток
        {
            countZero = 0;
            for (int x = 0; x < gamemap.Size; x++)
                for (int y = 0; y < gamemap.Size; y++)
                    if (gamemap.GetValue(x, y) == 0)
                        countZero++;

            return countZero;
        }
       
        public bool GameIsOver(Map gamemap) // метод на перевірку чи закінчилась гра
        {
            if (isGameEnd)
                return isGameEnd;
            else if (countZero > 0)
                return false;
            for (int x = 0; x < gamemap.Size; x++)
                for (int y = 0; y < gamemap.Size; y++)
                    if (map.GetValue(x, y) == map.GetValue(x + 1, y) || // перевірка чи немає поблизу однакоивх значень
                        map.GetValue(x, y) == map.GetValue(x, y + 1))   // щоб можна було ще зробити один хід
                        return false;
            IsGameEnd = true;
            return isGameEnd;
        }
        public void GameOver(int recordscore) // закінчення гри та збереження
        {
            Player.SetScore(recordscore);
            if (Player.Score > 0)
            {
                Leaderboard.AddToFile(Player);
            }
        }
    }
}
