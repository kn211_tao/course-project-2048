﻿using Newtonsoft.Json;

namespace FileClassLibrary
{
    public class Player
    {
        [JsonProperty]
        public string Name { get; }

        [JsonProperty]
        public int Score { get; set; }

        public Player(string name)
        {// конструктор
            Name = name;
            Score = 0;
        }

        // перевірка введення ім'я гравця
        public static bool TrySetName(string input, out string newName, out string errorMessage)
        {
            if (input.Trim() == "" || input.Trim() == null)
            {
                errorMessage = "Ви не можете бути порожнечею. Будь ласка, введіть ім'я";
                newName = null;
                return false;
            }
            else
            {
                errorMessage = null;
                newName = input.Trim();
                return true;
            }
        }

        // оновлення очок
        public void SetScore(int score)
        {
            if (score > 0)
            {
                Score += score;
            }
        }

        // скидання очок
        public void DropScoreToZero()
        {
            Score = 0;
        }
    }
}
