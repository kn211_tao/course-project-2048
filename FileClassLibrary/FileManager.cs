using System.IO;
using System.Text;

namespace FileClassLibrary
{
    public class FileManager
    {
        public static void Write(string fileName, string info)
        { // ����� � ����
            StreamWriter streamWriter = new StreamWriter(fileName, false, Encoding.UTF8);
            streamWriter.WriteLine(info);
            streamWriter.Close();
        }

        public static string Read(string fileName)
        { // ������� � �����
            try
            {
                StreamReader streamReader = new StreamReader(fileName, Encoding.UTF8);
                string result = streamReader.ReadToEnd();
                streamReader.Close();
                return result;
            }
            catch (FileNotFoundException)
            {
                return null;
            }
        }

        public static bool CheckFileExist(string fileName)
        { // ��������, �� ���� ����
            return File.Exists(fileName);
        }

    }
}
