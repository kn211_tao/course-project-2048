﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FileClassLibrary
{
    public class Leaderboard
    {
        // файл де зберігаються дані про таблицю лідерів
        private static readonly string fileName = "2048Leaderboard.json";

        public static void AddToFile(Player player)
        { // додання у файл
            List<Player> playerResults = GetAll();
            playerResults.Add(player);
            Save(playerResults);
        }

        public static List<Player> GetAll()
        { // отримання всіх даних з файлу в список
            if (!FileManager.CheckFileExist(fileName))
            {
                return new List<Player>();
            }
            string oldResults = FileManager.Read(fileName);
            List<Player> playerResults = JsonConvert.DeserializeObject<List<Player>>(oldResults);
            return playerResults;
        }

        public static List<Player> GetLast()
        {  // отримання останьонніх записів
            if (!FileManager.CheckFileExist(fileName))
            {
                return new List<Player>();
            }
            string all = FileManager.Read(fileName);
            List<Player> playerResults = JsonConvert.DeserializeObject<List<Player>>(all);
            return playerResults;
        }

        private static void Save(List<Player> playerResults)
        { // зберігання 
            string jsonData = JsonConvert.SerializeObject(playerResults);
            FileManager.Write(fileName, jsonData);
        }
    }
}
