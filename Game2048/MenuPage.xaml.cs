﻿using System.Windows;
using System.Windows.Controls;
using FileClassLibrary;

namespace Game2048
{
    public partial class MenuPage : Page
    {
        /// <summary>
        ///  сторінка з меню, навігація
        /// </summary>
        public MenuPage()
        {
            InitializeComponent();

            var lastPlayer = Leaderboard.GetAll();
            if (lastPlayer.Count > 0)
                textBox_playerName.Text = lastPlayer[lastPlayer.Count - 1].Name;
            else
                textBox_playerName.Text = "Ваше ім'я";
        }

        public MenuPage(MenuPage inputMenuPage)
        {
            InitializeComponent();
            textBox_playerName.Text = inputMenuPage.textBox_playerName.Text;
        }

        private void play_button_Click(object sender, RoutedEventArgs e) // натиск кнопки грати
        {
            string newCheckedName = "";
            if (!Player.TrySetName(textBox_playerName.Text, out newCheckedName, out string errorMessage))
                MessageBox.Show(errorMessage); // перевірка введеня імені гравця
            else
            {
                textBox_playerName.Text = newCheckedName;
                this.NavigationService.Navigate(new PlayPage(this));
            }        
        }
       
        private void leaderboard_button_Click(object sender, RoutedEventArgs e) // натиск кнопки таблиця лідерів
        {
            this.NavigationService.Navigate(new LeaderboardPage());
        }

        private void about_button_Click(object sender, RoutedEventArgs e) // натиск кнопки про гру
        {
            this.NavigationService.Navigate(new AboutGamePage());
        }

        private void exit_button_Click(object sender, RoutedEventArgs e) // натиск кнопки вихід
        {
            Application.Current.Shutdown();
        }
    }
}
