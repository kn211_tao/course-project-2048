﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using FileClassLibrary;

namespace Game2048
{
    public partial class LeaderboardPage : Page
    {
        public LeaderboardPage()
        {
            InitializeComponent();
           
            var userResultAll = Leaderboard.GetAll();

            if (userResultAll.Count > 0) // вивід даних на таблицю
            {
                for (int i = 0; i < userResultAll.Count; i++)
                {
                    CollectionViewSource itemCollectionViewSource;
                    itemCollectionViewSource = (CollectionViewSource)(FindResource("ItemCollectionViewSource"));
                    itemCollectionViewSource.Source = userResultAll;
                }
            }
            else
                return;
        }

        private void back_button_Click(object sender, RoutedEventArgs e) // натиск кнопки назад
        {
            this.NavigationService.Navigate(new MenuPage());
        }
    }
}
