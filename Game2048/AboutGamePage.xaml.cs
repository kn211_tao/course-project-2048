﻿using System.Windows;
using System.Windows.Controls;

namespace Game2048
{  
    public partial class AboutGamePage : Page
    {
        public AboutGamePage()
        {
            InitializeComponent();
        }

        private void back_button_Click(object sender, RoutedEventArgs e) // натиск кнопки назад
        {
            this.NavigationService.Navigate(new MenuPage());
        }

    }
}
