﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using WPFClassLibrary;
using FileClassLibrary;
using System.Windows.Media.Animation;
using Block = WPFClassLibrary.Block;

namespace Game2048
{
    /// <summary>
    /// сторінка з грою
    /// </summary>
    public partial class PlayPage : Page
    {
        static private MenuPage menuPage;

        DoubleAnimation DAnimation;
        Storyboard SBoard;

        public PlayPage(MenuPage inputMenuPage)
        {
            InitializeComponent();
            // налаштування для анімації
            DAnimation = new DoubleAnimation();
            DAnimation.Duration = TimeSpan.FromMilliseconds(250);
            SBoard = new Storyboard();
            SBoard.Children.Add(DAnimation);
            menuPage = inputMenuPage;
        }

        private void PlayPage_Loaded(object sender, RoutedEventArgs e)
        {
            var window = Window.GetWindow(this);
            window.KeyDown += Mgrid_KeyDown;
        }

        public Grid grid; // грід для карти
        private Map gamemap = new Map(); // карта
       
        private int RecordScore = 0; // рекордна кількість очок
        private int fontsize = 15; // розмір шрифту

        bool isSaved = false; // чи зберігалась гра
        private bool is2048exist = false; // чи існує плитка 2048

        GameLogic game = new GameLogic(4);
        GameLogic oldgame = new GameLogic(4);

        private void menu_button_Click(object sender, RoutedEventArgs e) // натиск кнопки меню
        {
            var window = Window.GetWindow(this);
            window.KeyDown -= Mgrid_KeyDown;
            if (!isSaved)
            { // збереження
                game.GameOver(RecordScore);
                isSaved = true;
            }
            this.NavigationService.Navigate(new MenuPage());
        }
        private void newgame_button_Click(object sender, RoutedEventArgs e) // натиск кнопки нова гра
        {
            NewGame();
        }
        private void gameover_button_Click(object sender, RoutedEventArgs e) // натиск кнопки завершити
        {
            if (!isSaved)
            {
                game.GameOver(RecordScore);
                isSaved = true;
            }
            GameWinLabel.Visibility = Visibility.Hidden;
            GameOverLabel.Visibility = Visibility.Visible;
        }

        private void mapSizeComboBox_Loaded(object sender, RoutedEventArgs e) // ініціалізація для вибору карти
        {
            List<string> data = new List<string>();
            data.Add("3x3");
            data.Add("4x4");
            data.Add("5x5");
            data.Add("6x6");
            data.Add("8x8");

            var dropdown = sender as ComboBox;

            dropdown.ItemsSource = data;
            dropdown.SelectedIndex = 1;
        }
        private void mapSizeComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e) // вибір карти
        {
            var dropdown = sender as ComboBox;

            string value = dropdown.SelectedItem as string;

            switch (value)
            {
                case "3x3":
                    gamemap.Size = 3;
                    fontsize = 60;
                    break;
                case "4x4":
                    gamemap.Size = 4;
                    fontsize = 50;
                    break;
                case "5x5":
                    gamemap.Size = 5;
                    fontsize = 40;
                    break;
                case "6x6":
                    gamemap.Size = 6;
                    fontsize = 30;
                    break;
                case "8x8":
                    gamemap.Size = 8;
                    fontsize = 20;
                    break;
            }

            NewGame();
        }

        private void NewGame() // ініціалізація нової гри
        {
            gamemap = new Map(gamemap.Size);
            is2048exist = false;
            game = new GameLogic(gamemap.Size, gamemap, new Player(menuPage.textBox_playerName.Text));
            oldgame = new GameLogic(game);
            game.Player.SetScore(0);
            game.AddRandomValue(ref gamemap);
            game.AddRandomValue(ref gamemap);
            GameOverLabel.Visibility = Visibility.Hidden;
            GameWinLabel.Visibility = Visibility.Hidden;
            DrawGridMap();

        }
        private void CheckGameState() // перевірка статусу гри (виграли / невиграли)
        {
            if (game.GameIsOver(game.map))
            {
                game.IsGameEnd = true;
                if (!isSaved)
                {
                    game.GameOver(RecordScore);
                    isSaved = true;
                }

                GameOverLabel.Visibility = Visibility.Visible;
            }
            else if (!is2048exist)
            {
                for (int x = 0; x < gamemap.Size; x++)
                    for (int y = 0; y < gamemap.Size; y++)
                        if (game.map.GetValue(x, y) == 2048)
                        {
                            GameWinLabel.Visibility = Visibility.Visible;
                            is2048exist = true;
                            game.GameOver(RecordScore);
                        }
            }
        }

        public void DrawGridMap() // промальовуємо карту 
        {
            myArea.Children.Remove(grid);

            grid = new Grid();
            grid.Name = "grid";
            grid.Width = 420;
            grid.Height = 420;
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.VerticalAlignment = VerticalAlignment.Center;

            for (int i = 0; i < gamemap.Size; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                grid.ColumnDefinitions.Add(col);
            }

            for (int i = 0; i < gamemap.Size; i++)
            {
                RowDefinition row = new RowDefinition();
                grid.RowDefinitions.Add(row);
            }

            for (int i = 0; i < gamemap.Size; i++)
            {
                for (int j = 0; j < gamemap.Size; j++)
                {
                    Thickness th = new Thickness(5); // для товщини країв
                    Button button = new Button();
                    button.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#CEC0B5"));
                    button.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#BBADA0"));
                    button.FontSize = fontsize;
                    
                    button.FontFamily = new FontFamily("AuX DotBitC Xtra Bold");
                    button.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#53463C"));
                    button.BorderThickness = th;
                    if (button.IsPressed == true)
                    {
                        button.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F27A5E"));
                        button.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F27A5E"));
                    }

                    if (gamemap.GetValue(i, j) != 0)
                    {
                        button.Content = gamemap.GetValue(i, j);
                        button.Background = gamemap.grid[i, j].GetTitleBlocksColor(gamemap.grid[i, j]);
                    }

                    // Анімація коли появляються нові плитки або коли вони з'єднуються
                    if (gamemap.grid[i, j].IsCombined == true || gamemap.grid[i, j].IsNewBlock == true)
                    {
                        int startfontsize = fontsize / 2;
                        int endfontsize = fontsize;
                        
                        if (gamemap.grid[i, j].Value >= 128)
                        {
                            endfontsize = fontsize - 5;
                            startfontsize = endfontsize / 2;
                            if (gamemap.grid[i, j].Value >= 256)
                            {
                                endfontsize = fontsize - 10;
                                startfontsize = endfontsize / 2;
                                if (game.map.Size == 8)
                                {
                                    endfontsize = fontsize - 6;
                                    startfontsize = endfontsize / 2;
                                }
                            }
                            if (gamemap.grid[i, j].Value >= 1000)
                            {

                                if (game.map.Size == 8)
                                {
                                    endfontsize = fontsize - 10;
                                    startfontsize = endfontsize / 2;
                                }
                                else if (game.map.Size == 6)
                                {
                                    endfontsize = fontsize - 13;
                                    startfontsize = endfontsize / 2;
                                }
                                else
                                {
                                    endfontsize = fontsize - 25;
                                    startfontsize = endfontsize / 2;
                                }
                                
                            }
                        }

                        DAnimation.From = startfontsize;
                        DAnimation.To = endfontsize;
                        Storyboard.SetTarget(DAnimation, button);
                        Storyboard.SetTargetProperty(DAnimation, new PropertyPath(Button.FontSizeProperty));
                        SBoard.Begin(button);
                    }

                    Grid.SetColumn(button, i);
                    Grid.SetRow(button, j);

                    grid.Children.Add(button);
                }

            }

            myArea.Children.Add(grid);
        }
        public void UpdateGridMap(Map gamemap) // оновлюємо карту
        {
            myArea.Children.Remove(grid);

            grid = new Grid();
            grid.Name = "grid";
            grid.Width = 420;
            grid.Height = 420;
            grid.HorizontalAlignment = HorizontalAlignment.Center;
            grid.VerticalAlignment = VerticalAlignment.Center;

            for (int i = 0; i < gamemap.Size; i++)
            {
                ColumnDefinition col = new ColumnDefinition();
                grid.ColumnDefinitions.Add(col);
            }

            for (int i = 0; i < gamemap.Size; i++)
            {
                RowDefinition row = new RowDefinition();
                grid.RowDefinitions.Add(row);
            }

            for (int i = 0; i < gamemap.Size; i++)
            {
                for (int j = 0; j < gamemap.Size; j++)
                {
                    Thickness th = new Thickness(5); // для товщини країв
                    Button button = new Button();
                    button.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#CEC0B5"));
                    button.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#BBADA0"));
                    button.BorderThickness = th;
                    //  розміри шрифтів
                    if (gamemap.grid[i, j].Value >= 128)
                    {
                        button.FontSize = fontsize - 5;
                        if (gamemap.grid[i, j].Value >= 256)
                        {
                            button.FontSize = fontsize - 10;
                            if(game.map.Size == 8)
                            {
                                button.FontSize = fontsize - 6;
                            }
                        }
                        if(gamemap.grid[i, j].Value >= 1000)
                        {
                            
                            if (game.map.Size == 8)
                            {
                                button.FontSize = fontsize - 10;
                            }
                            else if (game.map.Size == 6)
                            {
                                button.FontSize = fontsize - 13;
                            }
                            else
                               button.FontSize = fontsize - 25;
                        }
                    }
                    else button.FontSize = fontsize;
                    button.FontFamily = new FontFamily("AuX DotBitC Xtra Bold");
                    button.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#53463C"));
                   if(gamemap.grid[i, j].Value > 2048)
                        button.Foreground = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#FFFBF8F1"));

                    if (button.IsPressed == true)
                    {
                        button.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F27A5E"));
                        button.BorderBrush = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#F27A5E"));

                    }

                    if (gamemap.GetValue(i, j) != 0)
                    {
                        button.Content = gamemap.GetValue(i, j);
                        button.Background = gamemap.grid[i, j].GetTitleBlocksColor(gamemap.grid[i, j]);

                    }

                    // Анімація коли появляються нові плитки або коли вони з'єднуються
                    if (gamemap.grid[i, j].IsNewBlock == true || gamemap.grid[i, j].IsCombined == true)
                    {
                        int startfontsize = fontsize / 2;
                        int endfontsize = fontsize;

                        if (gamemap.grid[i, j].Value >= 128)
                        {
                            endfontsize = fontsize - 5;
                            startfontsize = endfontsize / 2;
                            if (gamemap.grid[i, j].Value >= 256)
                            {
                                endfontsize = fontsize - 10;
                                startfontsize = endfontsize / 2;
                                if (game.map.Size == 8)
                                {
                                    endfontsize = fontsize - 6;
                                    startfontsize = endfontsize / 2;
                                }
                            }
                            if (gamemap.grid[i, j].Value >= 1000)
                            {

                                if (game.map.Size == 8)
                                {
                                    endfontsize = fontsize - 10;
                                    startfontsize = endfontsize / 2;
                                }
                                else if (game.map.Size == 6)
                                {
                                    endfontsize = fontsize - 13;
                                    startfontsize = endfontsize / 2;
                                }
                                else
                                {
                                    endfontsize = fontsize - 25;
                                    startfontsize = endfontsize / 2;
                                }

                            }
                        }

                        DAnimation.From = startfontsize;
                        DAnimation.To = endfontsize;
                        Storyboard.SetTarget(DAnimation, button);
                        Storyboard.SetTargetProperty(DAnimation, new PropertyPath(Button.FontSizeProperty));
                        SBoard.Begin(button);
                    }

                    Grid.SetColumn(button, i);
                    Grid.SetRow(button, j);

                    grid.Children.Add(button);
                }
            }

            myArea.Children.Add(grid);
        }

        #region методи рухи плиток
        private void MoveUp(ref GameLogic game, ref GameLogic oldgame)     // рух вверх
        {

            if (Block.TryUp(ref game, ref oldgame) == true)  // якщо блоки змінились
            {

                //додаємо новий блок
                game.AddRandomValue(ref game.map);
                // оновлюємо карту
                UpdateGridMap(game.map);
                label_scorevalue.Content = game.Score.ToString();

                if (RecordScore < game.Score)
                {
                    RecordScore = game.Score;
                    label_bestscorevalue.Content = game.Score.ToString();
                }

            }
        }
        private void MoveDown(ref GameLogic game, ref GameLogic oldgame)   // рух вниз
        {
            oldgame.Score = game.Score;

            if (Block.TryDown(ref game, ref oldgame) == true)  // якщо рухи змінились
            {
                //додаємо новий блок
                game.AddRandomValue(ref game.map);
                // оновлюємо карту
                UpdateGridMap(game.map);
                label_scorevalue.Content = game.Score.ToString();

                if (RecordScore < game.Score)
                {
                    RecordScore = game.Score;
                    label_bestscorevalue.Content = game.Score.ToString();
                }

            }
        }
        private void MoveRight(ref GameLogic game, ref GameLogic oldgame)  // рух вправо
        {
            oldgame.Score = game.Score;

            if (Block.TryRight(ref game, ref oldgame) == true) // якщо рухи змінились
            {             
                //додаємо новий блок
                game.AddRandomValue(ref game.map);
                // оновлюємо карту
                UpdateGridMap(game.map);
                label_scorevalue.Content = game.Score.ToString();

                if (RecordScore < game.Score)
                {
                    RecordScore = game.Score;
                    label_bestscorevalue.Content = game.Score.ToString();
                }

            }
        }
        private void MoveLeft(ref GameLogic game, ref GameLogic oldgame)   // рух вліво
        {
            oldgame.Score = game.Score;

            if (Block.TryLeft(ref game, ref oldgame) == true) // якщо рухи змінились
            {
                //додаємо новий блок
                game.AddRandomValue(ref game.map);
                // оновлюємо карту
                UpdateGridMap(game.map);
                label_scorevalue.Content = game.Score.ToString();

                if (RecordScore < game.Score)
                {
                    RecordScore = game.Score;
                    label_bestscorevalue.Content = game.Score.ToString();
                }

            }
        }
        #endregion

        private void Mgrid_KeyDown(object sender, KeyEventArgs e) // зчитування натискання клавіши
        {
            GameOverLabel.Visibility = Visibility.Hidden;
            GameWinLabel.Visibility = Visibility.Hidden;
            switch (e.Key)
            {
                case Key.W:
                    MoveUp(ref game, ref oldgame);
                    break;
                case Key.S:
                    MoveDown(ref game, ref oldgame);
                    break;
                case Key.D:
                    MoveRight(ref game, ref oldgame);
                    break;
                case Key.A:
                    MoveLeft(ref game, ref oldgame);
                    break;
                case Key.Up:
                    MoveUp(ref game, ref oldgame);
                    break;
                case Key.Down:
                    MoveDown(ref game, ref oldgame);
                    break;
                case Key.Right:
                    MoveRight(ref game, ref oldgame);
                    break;
                case Key.Left:
                    MoveLeft(ref game, ref oldgame);
                    break;
                default:
                    break;
            }
            CheckGameState();
        }        
    }
}
