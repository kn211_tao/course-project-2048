﻿using System.Windows;

namespace Game2048
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += mainwindow_Loaded;
        }

        private void mainwindow_Loaded(object sender, RoutedEventArgs e) // завантаження головного вінка
        {
           mainframe.NavigationService.Navigate(new MenuPage()); // перехід в меню
        }

    }
}


